\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1300 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 26 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

% Taken from first answer to
% https://tex.stackexchange.com/questions/134863/command-for-transverse-and-not-pitchfork-as-used-in-guillemin-and-pollack
\newcommand{\transv}{\mathrel{\text{\tpitchfork}}}
\makeatletter
\newcommand{\tpitchfork}{%
  \vbox{
    \baselineskip\z@skip
    \lineskip-.52ex
    \lineskiplimit\maxdimen
    \m@th
    \ialign{##\crcr\hidewidth\smash{$-$}\hidewidth\crcr$\pitchfork$\crcr}
  }%
}
\makeatother

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator{\Id}{id}

\begin{document}

\maketitle

\begin{theorem}[Sard]
Let \(f: X \to Y\) be smooth. Then the set of critical values of \(f\) has measure \(0\)
\end{theorem}
We begin by recalling some properties of sets of measure zero.
\begin{definition}
\(A \subseteq \reals^n\) has measure zero if, given \(\epsilon \in \reals^+\), there exist a countable collection \(\mc{C}\) of boxes \(S_i\) such that
\[\bigcup_i S_i \supseteq A \land \sum_i V(S_i) \leq \epsilon\]
\end{definition}
If each \(A_i\) has measure zero, for some countable collection, then
\[\bigcup_iA_i\]
has measure zero since we can cover each \(A_i\) with a set of boes having volume \(\leq \epsilon/2^i\), since
\[\sum_i\frac{\epsilon}{2i} < \epsilon\]
If \(f: \reals^n \to \reals^n\) is smooth and \(A \subseteq \reals^n\) has measure zero, then \(f(A)\) has measure zero. Too see why, divide \(\reals^n\) into countably many compact sets \(B_i\) (say a coordinate grid). Define
\[A_k = A \cap B_k\]
By the first property we discussed, it is enough to show each \(A_k\) has measure zero. Since \(f\) is smooth, \(f|_{B_k}\) is Lipschitz and hence
\[|f(x) - f(y)| \leq M|x - y|\]
Hence the box \(B_k\) is scaled up by at most \(M\), and so it's volume scales up by at most \(M^n\), and the argument follows from this fact.
Now assume we have \(m > n\), and embed \(\reals^n \subseteq \reals^m\). We claim \(\reals^n\) has measure zero in \(\reals^m\). To do this, we split up \(\reals^n\) into boxes, and extend into \(\reals^m\). Each box of \(\reals^n\) can be ``squished" along one of the \(m - n\) axes to be made arbitrarily small, implying it is of measure 0. Hence \(\reals^n\) is of measure 0.

Now let's generalize! Let \(X \subseteq \reals^n\) be a manifold with dimension \(n\). Then \(X\) has measure 0 in \(\reals^m\). To prove this, we have to do a little more work, but, we can in essense split up \(X\) into countably many chunks, each one being the image of some function \(\varphi: U \subseteq \reals^n \to \reals^m\). We embed \(\reals^n\) into \(\reals^m\), and hence have that \(X\) is composed of (countably many) images of a measure zero set through a smooth function \(f: \reals^m \to \reals^m\) given by the composition of the embedding of \(\reals^n\) and the coordinate chart \(\varphi\), giving, by the above facts, that \(X\) itself is of measure zero.

For each \(c \in \reals\), define \(A_c = A \cap \{c\} \times \reals^{n - 1}\). If \(A_c\) has measure 0 in \(\reals^{n - 1}\), then \(A\) has measure 0 in \(\reals^n\).
Assume \(A\) is compact. If \(\{S_i\}\) is a covering of \(A_c\) with open boxes, then there exists an interval \(J(c)\), \(c \in J\), such that
\[\bigcup J \times S_i \supseteq A \cap J \times \reals^{n - 1}\]

\TODOD{this section}

We now move on to a key special case of Sard's theorem.
\begin{claim}
Let \(U \subseteq \reals^n\), and \(f: U \to \reals^n\). Let \(C\) be the set of critical values of \(f\). \(C\) has measure \(0\) in \(\reals^n\)
\end{claim}
\begin{proof}
  Define \(C_k\) to be the set of points \(\in U\) such that all \(k\)-derivatives of \(f\) are 0. We want to show that \(C \setminus C_1\), \(C_k \setminus C_{k + 1}\) has measure zero, and that \(C_k\) has measure zero when \(k\) is very large.

  We'll provide a sketch of each:
  \begin{enumerate}

    \item \(C \setminus C_1\) is of measure zero:

    \TODOD{this}

    \item \(C_k \setminus C_{k + 1}\) is of measure zero:
    Say \(x \in C_k \setminus C_{k + 1}\). Then \(\exists\rho\) which is a \(k^{th}\) derivative of \(f\) such that \(\rho(x) = 0\), but \(\prt{\rho}{x}|_x \neq 0\). Define
    \[h(x_1,...,x_n) = (\rho(x_1),x_2,...,x_n)\]
    Define
    \[f' = f \circ h^{-1}\]
    Note that this is a local diffeomorphism.

    \TODOD{rest}

    \item \(C_k\) has measure zero when \(k\) is very large:

    \TODOD{this}

  \end{enumerate}
\end{proof}

\end{document}
