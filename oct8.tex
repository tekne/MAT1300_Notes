\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1300 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 8 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

% Taken from first answer to
% https://tex.stackexchange.com/questions/134863/command-for-transverse-and-not-pitchfork-as-used-in-guillemin-and-pollack
\newcommand{\transv}{\mathrel{\text{\tpitchfork}}}
\makeatletter
\newcommand{\tpitchfork}{%
  \vbox{
    \baselineskip\z@skip
    \lineskip-.52ex
    \lineskiplimit\maxdimen
    \m@th
    \ialign{##\crcr\hidewidth\smash{$-$}\hidewidth\crcr$\pitchfork$\crcr}
  }%
}
\makeatother

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator{\Id}{id}

\begin{document}

\maketitle

The exam will be up to the end of what we teach today. And today, we want to do some topology. We discussed the notion of homotopy, and often, we consider maps up to homotopy. If we want to say that \(f\) has some property, we want to say it has some property up to homotopy.

Say we have a space \(X \subseteq Y\). We can treat \(X\) as a map \(i: X \to Y\), and from there talk about homotopy. For example, we could have a annulus \(\alpha\) and a ring around the center \(\beta\), and we want to see if we can deform \(\alpha\) to ``lift" it off \(\beta\)

We know \(I(\alpha, \beta) = 1\), but homotopies can change intersecton numbers: for example, if we have that \(X\) and \(Z\) intersect non-transversally (implying no ``real" intersections), with \(Z\) being a horizontal line and \(X\) being ``cubic like", we can ``wiggle" \(Z\) to get a transversal intersection \textit{or} get \textit{three} transversal intersections. We can even go higher. So we need to do something more clever.
\begin{definition}
  Define \(I_z(f, z) = \ \text{intersection number of} \ g(x), z \mod 2 = |f(X) \cap Z| \mod 2\) where \(g \transv Z\), \(g \simeq f\).
\end{definition}
This is a pretty weak property, that we'll extend after the exam, but it can already help us a lot
\begin{theorem}
  \(I_z(f, z)\) is well-defined.
\end{theorem}
\begin{proof}
  We need to show that if \(g_0 \simeq g_1\), \(g_0, g_1 \transv Z\), and
  \[|g_0(X) \cap Z| \equiv  |g_1(X) \cap Z| \mod 2\]
  We can find a homotopy \(G: X \times I \to Y\) such that
  \[G(x, 0) = g_0(x), \qquad G(x, 1) = g_1(X), \qquad \partial G: \partial(X \times I) \to Y \transv Z\]
  We have
  \[\partial(X \times I) = X \times \{0\} \cup X \times \{1\}\]
  By the Extension Theorem, we have that we can find \(F: X \times I \to Y\) such that \(\partial F = \partial G\) and \(F \transv G\). We have that \(F^{-1}(Z)\) is amanifold. Furthermore,
  \[\partial(F^{-1}(Z)) = F^{-1}(Z) \cap \partial(X \times I) = g_0^{-1}(Z) \cup g_1^{-1}(Z)\]
  Since \(|g_0^{-1}(Z)| + |g_1^{-1}(Z)|\) is even, they are the same mod 2.
\end{proof}
So going back to our example, we can't change one intersection to no intersections, since they are non-equal mod 2.

We'll now consider something else: self-intersection numbers. Consider the Mobius strip, and a circle \(X\) along it (given as a line bisecting the square defining it). What is \(I_2(X, X)\)? That is, can \(X\) be homotopped off itself? For the annulus \(\alpha\), this is very easy, since we can just slide it along the torus. But what about in this case? Clearly, the identity \(i: X \to X\) is not transverse to \(X\), but if we deform \(X\) to a ``logistic curve" on the defining square, it now has one intersection, giving \(I_2(X, X) = 1\). Since the answer is odd, it's impossible to make it be even, and hence impossible to make it be zero.

We want to use the fact that this intersection number is well-defined to define another important number, degree. As last time, we'll start with the lazy case of degree mod 2. The intuitive idea of degree is ``how many times one manifold covers another."
\begin{definition}
  Let \(f: X \to Y\) where \(X\) is a compact manifold with no boundary, \(Y\) is connected and \(\dim(X) = \dim(Y)\). Define
  \[\deg_2(X) = \#(f^{-1}(y)) \ \text{for any regular value} \ y \ \text{of} \ f \mod 2 = I_2(f, \{y\})\]
\end{definition}
\begin{theorem}
\begin{enumerate}
  \item \(\deg_2(f)\) is well-defined.
  \item Both definitions are the same
  \item If \(f \simeq g\), \(\deg_2(f) = \deg_2(g)\).
\end{enumerate}
\end{theorem}
\begin{proof}
  3 follows immediately from the second definition, via our theorem about \(I_2\). The two definitions are the same if \(y\) is a regular value. So it remains to show that \(I_2(f, \{y\})\) does not depend on \(y\). So how do we prove a function is constant on a connected set? In this case, all we need to do is show that \(I_2(f, \{y\})\) is locally constant.

  Find \(g \simeq f\), such that \(g \transv \{y\}\). We have that \(\#g^{-1}(y)\) is locally constant, and by the ``stack of records" theorem:
  \[
    g^{-1}(U) = \bigcup_{i = 1}^kV_i
  \]
  i.e. there exists a neighborhood \(U\) of \(\{y\}\) and \(V_i\) for each \(x_i \in g^{-1}(y)\) such that \(g|_{V_i}V_i \to U\) is a diffeomorphism.

  Therefore, for any \(y' \in U\), \(\#g^{-1}(y) = \#g^{-1}(y)\). Hence \(I_2(g, \{y\}) = I_2(g, \{y'\})\), i.e. \(I_2(g, \{y\})\) is locally constant.
\end{proof}
\begin{theorem}[The Boundary Theorem]
Let \(X = \partial W\) where \(W\) is compact, and \(g: X \to Y\), where \(g\) extends to \(W\), and \(Z \subseteq Y\) is closed with complementary dimension to \(X\). This implies \(I_2(g, Z) = 0\).
\end{theorem}
\begin{proof}
  There exists \(G: W \to Y\) such that \(\partial G = g\). Furthermore, there exists \(F: W \to Y\) such that \(F \simeq G\), \(\partial F \simeq g\), \(F \transv Z\) and \(\partial F \transv Z\).
  From this, we have that \(F^{-1}(Z)\) is a manifold of codimension \(\dim(X)\), i.e. that \(F^{-1}(Z)\) is a one-manifold. Hence \(\partial F^{-1}(Z)\) is an even number of points, giving that \(\#F^{-1}(Z)\) is given and hence \(I_2(g, Z) = 0\).
\end{proof}
Pick \(p(z) = z^m + a_1z^{m - 1} + a_2z^{m - 2} + ... + a_m\). It'll send a large circle around the origin to another large circle which goes around \(m\) times.
\begin{theorem}
  If \(X = \partial W\), where \(W\) is compact, and \(f; X \to Y\) where \(\dim(X) = \dim(Y)\) can be extended to \(W\), then \(\deg_2(f) = 0\)
\end{theorem}
\begin{proof}
  Apply the boundary theorem for \(Z = \{y\}\) for \(y \in Y\).
\end{proof}
Hence, if \(\deg_2(f) \neq 0\), there does \textit{not} exist an extension.

Let \(S_R\) be the circle of radius \(R\) in \(\comps\). We define a map \(f: S_R \to S^1\) by choosing \(f(z) = \frac{p(z)}{|p(z)|}\). This will be a nice map if \(p(z)\) is not zero, which is true for \(R\) large enough. We want to show that
\[f \simeq \left(z \mapsto \frac{z}{|z^m|}\right)\]
We define
\[f_t(z) = tp(z) + (1 -t)z^m\]
We have that
\[F(t, z) = \frac{f_t(z)}{|f_t(z)|}\]
is a homotopy between the desired functions. We need to check that \(f_t(z)\) is \underline{not} zero for \(z \in S_R\). To do so, note that
\[\frac{f_t(z)}{z^m} = t\frac{p(z)}{z^m} + (1 - t)\frac{z^t}{z^m} = 1 + \sum_{i = 1}^m\frac{a_i}{z^i} \neq 0\]
for large enough \(R\). Hence,
\[\deg_2(p(z)/|p(z)|) = \deg_2(z^m/|z^m|) = m \mod 2\]
as desired.

For now we proved the weak version, but it later will actually work without the ``\(\mod 2\)". 

\end{document}
