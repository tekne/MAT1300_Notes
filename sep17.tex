\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1300 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 17 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}

\begin{document}

\maketitle

TODO: beginning material...

\subsection*{Manifold Examples}

Let's consider some examples of manifolds as subsets of \(\reals^m\):
\begin{itemize}

  \item Consider \(\reals^n \subseteq \reals^m\), that is \(\reals^n\) considered as a sub-manifold of \(\reals^m\).

  \item The one-dimensional sphere \(S^1 \subseteq \reals^2\), a.k.a. the circle. We can think of it in two different ways:
  \begin{itemize}
    \item We can write it in \(\reals^2\) as
    \[S^1 = \{(x_1, x_2) \in \reals^2 : x_1^2 + x_2^2 = 1\}\]
    Thought of in this way it readily generalizes to the \(n\)-dimensional sphere
    \[S^n = \left\{(x_1,...,x_{n + 1}) \in \reals^{n + 1} : \sum_ix_i^2 = 1\right\}\]
    \item We can write it in \(\comps\) as
    \[S^1 = \{z \in \comps : |z| = 1 \}\]
    Thought of in this way it readily generalizes to the \(n\)-dimensional torus
    \[T^n = \{(z_1,...,z_n) \in \comps^n : |z_1| = ... = |z_n| = 1\}\]
  \end{itemize}
  So \(S^1\) can be thought of as both the one-dimensional sphere and one-dimensional torus.

  \item Let's consider the tangent space of \(\mbb{T}^2\). Taken in \(\comps^2\), we can easily parametrize the torus by
  \[t: U = (0, 2\pi) \times (0, 2\pi) \to \comps^2\]
  where
  \[t(\theta, \varphi) = (e^{i\theta}, e^{i\varphi})\]
  On the other hand, for \(\mbb{T}^2 \subseteq \reals^3\), we get that the ``center" of the circular cross-sections of the torus at each angle \(\theta\) is given by
  \[c: (0, 2\pi) \to \reals^3, c(\theta) = (2\cos\theta, 2\sin\theta, 0)\]
  To ``expand" the center to the torus, we have points, for each angle \(\varphi\), a point
  \[\cos\varphi(\cos\theta, \sin\theta, 0) + \sin\varphi(0, 0, 1)\]
  away from the center, giving parametrization
  \[t: U \to \reals^3, t(\theta, \varphi) = c(\theta) + \cos\varphi(\cos\theta, \sin\theta, 0) + \sin\varphi(0, 0, 1) = (\cos\theta(2 + \cos\varphi), \sin\theta(2 + \cos\varphi), \sin\varphi)\]
  This covers the torus but misses a single cross-section and an outer diameter, incidentally, since it is a diffeomorphism, it shows that this shape is diffeomorphic to \(U\) and hence \(\reals^2\).

  However, this is enough to start talking about the tangent space. Let's say we pick a particular point on the parametrization that we like, call it \(x\).

  In general, if an open \(U \subseteq \reals^n\) and \(X \subseteq \reals^m\), if we have a map \(f: U \to X\), and \(f(p) = x\), then
  \[df_p = \reals^n \to \reals^m\]
  This means that, if we write, for \(f_i: U \to \reals\),
  \[f = (f_1,...,f_m)\]
  then
  \[df = \begin{pmatrix}
    \prt{f_1}{x_1} & ... & \prt{f_1}{x_n} \\
    \vdots & \ddots & \vdots \\
    \prt{f_m}{x_n} & ... & \prt{f_m}{x_n}
  \end{pmatrix} \in \reals^{m \times n}\]
  We then simply have
  \[T_x(X) = df_p(\reals^n)\]

  Back to our concrete case, let's say we have \(x = (-1, 0, 0)\). What's \(T_x\mbb{T}^2\)? We have, for \(p = (\pi, \pi)\),
  \[f(p) = (-1(2 - 1), 0(2 - 1), 0) = (-1, 0, 0) = x\]
  We have
  \[df = \begin{pmatrix}
    -\sin\theta(2 + \cos\varphi)  & -\cos\theta\sin\varphi  \\
    \cos\theta(2 + \cos\varphi)   & -\sin\theta\sin\varphi  \\
    0                             & \cos\varphi
  \end{pmatrix}\]
  Hence,
  \[df_p = \begin{pmatrix} 0 & 0 \\ 3 & 0 \\ 0 & 1 \end{pmatrix}\]
  This gives
  \[T_x\mbb{T}^2 = df_p(\reals^2) = \Span\begin{pmatrix} 0 & 0 \\ 3 & 0 \\ 0 & 1 \end{pmatrix}\]
  which is the \(yz\)-plane. Looks right to me.

  \item Let's do one more calculation for \(S^2\). We're going to try to parametrize it in two ways and see that the tangent spaces are the same. Let's start with spherical coordinates:
  \[f: (-\pi, \pi) \times (-\pi/2, \pi/2) \to S^2, f(\theta, \varphi) = (\cos\varphi\cos\theta, \cos\varphi\sin\theta, \sin\varphi)\]
  This covers most of the sphere, except the poles and the semi-circle passing through them at \(\pi\)/\(-\pi\).
  We have
  \[df = \begin{pmatrix}
    -\cos\theta\sin\theta & -\sin\varphi\cos\theta \\
    \cos\varphi\cos\theta & -\sin\varphi\sin\theta \\
    0                     & \cos\varphi
  \end{pmatrix}\]
  Alternatively, we can try the stereographic projection from \(\reals^2\),
  \[(x, y) \mapsto \left(\frac{2x}{x^2 + y^2 + 1}, \frac{2y}{x^2 + y^2 + 1}, \frac{x^2 + y^2 - 1}{x^2 + y^2 + 1}\right)\]
  or, somewhat more easily, from polar coordinates (call this one \(g\))
  \[(r, \theta) \mapsto\left(\frac{2r\cos\theta}{r^2 + 1}, \frac{2r\sin\theta}{r^2 + 1}, \frac{r^2 - 1}{r^2 + 1}\right)\]
  This gives
  \[dg = \begin{pmatrix}
    -\frac{2r\sin\theta}{r^2 + 1} & \frac{1 - r^2}{(1 + r^2)^2}2\cos\theta  \\
    \frac{2r\cos\theta}{r^2 + 1}  & \frac{1 - r^2}{(1 + r^2)^2}2\sin\theta  \\
    0                             & \frac{4r}{(1 + r^2)^2}
  \end{pmatrix}\]
  Let's keep it simple and choose \(x = (1, 0, 0)\). For \(f\), we have \(p = (0, 0)\). Then
  \[T_xS^2 = df_p(\reals^3)= \Span\begin{pmatrix} 0 & 0 \\ 1 & 0 \\ 0 & 1 \end{pmatrix}\]
  Similarly, for \(g\), we have \(p = (1, 0)\), giving
  \[T_xS^2 = dg_p(\reals^2) = \Span\begin{pmatrix} 0 & 0 \\ \frac{2}{2} & 0 \\ 0 & \frac{4}{4} \end{pmatrix}\]
  which, naturally, is equal. So there's an engineering proof that the tangent spaces are equal!

\end{itemize}

\section*{Immersion and Submersion}

Consider a map \(f: X \to Y\), where \(X\), \(Y\) are manifolds parametrized by open \(U \subseteq \reals^n\) and \(V \subseteq \reals^m\). We have
\[df: TX \to TY, \qquad df_x: T_xX \to T_{f(x)}Y\]
As diagrams, we have that, if \(g: U \to V\), \(X\) is parametrized by \(\varphi_1: U \to X\), \(\varphi_2: V \to Y\)
\[
\begin{tikzcd}
  \reals^n \supseteq X \arrow[r, "f"] & Y \subseteq \reals^M \\
  \reals^k \supseteq U \arrow[u, "\varphi_1"] \arrow[r, "g"] & V \subseteq \reals^m \arrow[u, "\varphi_2"]
\end{tikzcd}
\]
inducing the mapping between the tangent spaces
\[
\begin{tikzcd}
  \reals^N \supseteq T_xX \arrow[r, "df_x"] & T_{f(x)}Y \subseteq \reals^M \\
  \reals^n \arrow[u, "d\varphi_1"] \arrow[r, "dg_{p}"] & \reals^m \arrow[u, "d\varphi_2"]
\end{tikzcd}
\]
\begin{definition}
\(x \in X\) is a \textbf{regular point} if \(df_x\) has maximal rank.
\end{definition}
\begin{definition}
\(y \in Y\) is a \textbf{regular value} if for every \(x \in f^{-1}(y)\), we \(df_x\) has maximal rank (i.e. \(x\) is a regular point).
\end{definition}
\begin{definition}
In both immersions and submersions, the derivaitive has maximal possible rank at every point \(x\), i.e. every point \(x\) is a regular point. If \(df\) is a bijection, we have an immersion, otherwise, if the dimension of the target space is too big, \(df\) is a surjection and we have a submersion.
\end{definition}
\begin{theorem}[Local Immersion Theorem]
If \(f: X \to Y\) is an immersion then locally the image is a submanifold of \(Y\).
\end{theorem}
To obtain a global version, we need that \(f\) is one to one.
\begin{theorem}[Local Submersion Theorem]
If \(f: X \to Y\) is a submersion, then for each \(x \in X\), there exists \(U \ni x\) such that
\[U \cap f^{-1}(f(x))\]
is a submanifold of \(X\).
\end{theorem}
\begin{definition}
If \(x \in X\) is not a regular point, then it is a \textbf{critical point}
\end{definition}
\begin{theorem}[Sard's Theorem]
For any map \(f: X \to Y\), the measure of the set of critical values of measure \(0\).
\end{theorem}

\end{document}
