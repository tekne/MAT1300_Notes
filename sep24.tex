\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1300 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 24 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

% Taken from first answer to
% https://tex.stackexchange.com/questions/134863/command-for-transverse-and-not-pitchfork-as-used-in-guillemin-and-pollack
\newcommand{\transv}{\mathrel{\text{\tpitchfork}}}
\makeatletter
\newcommand{\tpitchfork}{%
  \vbox{
    \baselineskip\z@skip
    \lineskip-.52ex
    \lineskiplimit\maxdimen
    \m@th
    \ialign{##\crcr\hidewidth\smash{$-$}\hidewidth\crcr$\pitchfork$\crcr}
  }%
}
\makeatother

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator{\Id}{id}

\begin{document}

\maketitle

\begin{definition}
  \[\codim_Y(X) = \dim(Y) - \dim(X)\]
\end{definition}
\begin{theorem}[Pre-Image Theorem]
  Let \(f: X \to Y\) be smooth (where \(X, Y\) are smooth manifolds) and \(y \in Y\) be a regular value for \(f\). Then \(f^{-1}(y)\) is a submanifold of \(X\) of dimension \(\dim(X) - \dim(Y)\).
\end{theorem}
\begin{corollary}
  Let \(Z = f^{-1}(y)\) where \(X, Y, f, y\) are as in the theorem, and let \(x \in Z\) (\(\implies f(x) = y\)). Then
  \[T_x(Z) = \ker(df_x)\]
  (note \(df_x: T_x(X) \to T_y(Y)\))
\end{corollary}
\begin{proof}
  \(f\) is constant on \(Z\), so \(\forall v \in T_x(Z), df_x(v) = 0\), implying that \(T_x(Z) \subseteq \ker(df_x)\). By the pre-image theorem, they have the same dimension, and hence, the spaces are equal.
\end{proof}
So that's our warm-up from last week. Now let's talk about transversality. We want to formalize the notion that
\begin{itemize}
  \item A line going down through the plane is a ``good" intersection
  \item A parabola touching the plane in parallel is a ``bad" intersection
\end{itemize}
We can talk about this in terms of ambient spaces, but what's more interesting is the topological notion of ``stability:" we can pull the parabola ``off" the plane, but no matter how we move the line it goes through.
\begin{definition}
\(f: X \to Y\) is transversal to \(Z \subseteq Y\) if \(\forall y = f(x) \in Z\),
\[df_x(T_x(X)) \oplus T_y(Z) = T_y(Y)\]
or, equivalently, if we write \(Z\) (locally) as a zero set of \(g: Y \to \reals^\ell\), \(f: X \to Y\) is transversal to \(Z\) if \(0\) is a regular value for \(g \circ f\) at every point \(z \in Z\).
\end{definition}
\begin{theorem}
If \(f: X \to Y\) is transversal to \(Z \subseteq Y\), then \(f^{-1}(Z)\) is a submanifold of \(X\) with
\[\codim_X(f^{-1}(Z)) = \codim_Y(Z)\]
\end{theorem}
\begin{proof}
We check that \(f^{-1}(Z)\) is a manifold in a neighborhood \(U\) of \(x \in X\). Define \(y = f(x)\), \(g: Y \to \reals^\ell\) such that \(Z\) is the zero set of \(g\) around \(f(x)\). Then by definition \(0\) is a regular value for \(g \circ f: X \to \reals^\ell\), meaning
\[(g \circ f)^{-1}(0) \cap U = f^{-1}(Z) \cap U\]
is a manifold with dimension \(\dim(X) - \ell\).
\end{proof}
\begin{definition}
For \(X, Z \subseteq Y\), we say that \(X\) is transverse to \(Z\), written \(X \transv Z\), if
\begin{enumerate}

  \item \(\Id: X \to Y\) is transverse to \(Z\)

  \item For any \(y \in X \cap Z\), we have \(T_y(X) + T_y(Z) = T_y(Y)\)

\end{enumerate}
\end{definition}
\begin{corollary}
  If \(X \transv Z\), then \(X \cap Z\) is a submanifold of \(Y\), and
  \[\codim_Y(X \cap Z) = \codim_Y(X) + \codim_Y(Z)\]
\end{corollary}


\end{document}
