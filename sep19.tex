\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1300 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 19 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}

\begin{document}

\maketitle

Consider the diagram
\[
\begin{tikzcd}
x \in X \arrow[r, "f"] & y = f(x) \in Y \\
\reals^\ell \supseteq U \arrow[u, "\varphi"] \arrow[r, "G"] & V \arrow[u, "\psi"]
\end{tikzcd}
\]
If \(f\) is an immersion, then after a change of coordinates, we have
\[G(p) = (g(p),0,...,0)\]
where the last \(k - \ell\) dimensions are 0. If \(f\) is a submersion, then we get
\[G(p, c_1,...,c_{k - \ell}) = g(p)\]
We have that \(y\) is a regular value of \(f\) if
\[\forall z \in f^{-1}(y), df_: T_z(X) \to T_y(Y) \text{ is onto }\]
In this case \(f^{-1}(y)\) is a submanifold of \(x\) with co-dimension \(\dim(Y)\), that is
\[\dim(X) = \dim(f^{-1}(y)) + \dim(Y)\]
Consider now
\[g_1,g_2,...,g_\ell: X \to \reals\]
and let
\[Z = \text{ the zero set of all } g_i = \{x = X : \forall i, g_i(x) = 0\}\]
Then \(Z\) is a submanifold of \(X\) of co-dimension \(\ell\) if \(\{g_i\}\) are ``independent".
In the case where \(X = \reals^n\), and \(g_1,...,g_\ell\) are linear, then we equivalently require
\begin{itemize}
  \item \(\dim\left(\bigcap_ig_i^{-1}(0)\right) = k - \ell\)
  \item The function from \(\reals^n \to \reals^\ell\) given by \((x) \mapsto (g_1(x),...,g_\ell(x))\) is onto
  \item \(\{\ker(g_i)\}\) are linearly independent
\end{itemize}
In general, we require equivalently that
\begin{itemize}
  \item \(dg_i\) are linearly independent in the co-tangent space \(T^*_x(X)\) (which we haven't defined yet) for every \(x \in Z\)
  \item For \(g(x) = (g_1(x),...,g_\ell(x)): X \to \reals^\ell\), 0 is a regular value of \(g\)
  \item \(\ker(dg_1),...,\ker(dg_l)\) are linearly independent in \(T_x(x)\) for all \(x \in Z\).
\end{itemize}
Each time one new version of this shows up, you shouldn't get confused: they're all ways of saying the same thing.

We now want to show that \(O(n)\), the set of orthogonal \(n \times n\) matrices, is a smooth manifold. Define the set \(M(n) = \reals^{n \times n}\). We have that \(O(n) \subseteq M(n)\) can easily be described by
\[O(n) = \{A : AA^t = I\}\]
So, let's start by figuring out \(\dim(O(n))\). Obviously, we have \(\dim(M(n)) = n^2\).
There is a map from \(M(n)\) into the symmetric matrices \(S(n) = \{A : A = A^t\}\) given by
\[\Phi(A) = AA^t\]
We claim that \(I\) is a regular value for \(\Phi\). This would imply that \(O(n)\) is a smooth manifold (since it is the preimage of \(I\) in \(\Phi\) by definition) and that
\[\dim(O(n)) = \dim(M(n)) - \dim(S(n)) = n^2 - \frac{n^2 + n}{2} = \frac{n^2 - n}{2}\]
So let's see if we can actually do this.
We have, for some \(A \in M(n), S \in S(n)\)
\[T_A(M(n)) = M(n), T_S(S(n)) = S(n)\]
Hence, for \(A, B \in M(n)\), we have by definition (note here the tangent space is identified with the space itself) that
\[d\Phi_A(B) = \lim_{h \to 0}\frac{\Phi(A + hb) - \Phi(A)}{h} = \lim_{h \to 0}\frac{(A + hb)(A + hb)^t - AA^t}{h}\]
\[= \lim_{h \to 0}\frac{\cancel{AA^t} + hBA^t + hAB^t + \cancelto{0}{h^2BB^t} - \cancel{AA^t}}{h} = BA^t + AB^t\]
Assume now that \(AA^t = I\). We need to show that \(d\Phi_A\) is onto, i.e.
\[\forall C \in S(n), \exists B \in M(n), d\Phi_A(B) = BA^t + AB^t = C\]
Try \(B = \frac{CA}{2}\). Then
\[BA^t + AB^t = \frac{CAA^t + AA^tC^t}{2} = \frac{CI + IC^t}{2} = C\]
since \(C\) is symmetric. This gives the desired result.

Now that everyone is happy for all this, let's define transversality. So far, our problem has been that, if we have a map to \(\reals^n\), and we have the preimage of a point, how do we know it's a manifold? We generalize this to that if we have \(f: X \to Y\), and \(Z\) is a submanifold of \(Y\), how do we know if \(f^{-1}(Z)\) is a manifold?
The idea is that we can think of the manifold \(Z\) as the zero set of a bunch of functions. So the answer is, very heuristically, yes, the pre-image is a submanifold of \(X\) when \(df\) fills out the complement of \(Z\).

Let's develop this heurstic by considering a good situation and a bad situation:
\begin{itemize}

  \item Good situation: assume we have \(X = \reals^2\) (or some 2D manifold), and \(Y \subseteq \reals^3\), and the pre-image of the submanifold of \(Y\) becomes a line in \(X\)

  \item Bad situation: say we have two 2D manifolds but the tangent space of \(f(X)\) and \(Y\) are... TODO

\end{itemize}
From this, we come to the concept of transversality: if the dimension of two things is less than the ambient dimension, you can never be transversal...
We can write this in several different ways. One of them is, if we have \(f: X \to Y\), and a submanifold of \(Y\) \(Z\), write \(Z\) as the zero set of \(g = (g_1,...,g_\ell)\). Then
\[Z = \{y \in Y : g(y) = 0\}\]
Then \(f(X)\) is transversal to \(Z\) in \(Y\) if 0 is a regular value for \(g \circ f\). You can always find correct coordinates that a submanifold is locally a zero set, so finding \(g\) is not a problem.
The picture is clear: sets are transversal if they intersect as little as possible. Deformation and so on will come into the picture.

\end{document}
