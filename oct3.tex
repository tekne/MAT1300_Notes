\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT1300 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 3 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}
\newcommand{\prt}[2]{{\frac{\partial #1}{\partial #2}}}

% Taken from first answer to
% https://tex.stackexchange.com/questions/134863/command-for-transverse-and-not-pitchfork-as-used-in-guillemin-and-pollack
\newcommand{\transv}{\mathrel{\text{\tpitchfork}}}
\makeatletter
\newcommand{\tpitchfork}{%
  \vbox{
    \baselineskip\z@skip
    \lineskip-.52ex
    \lineskiplimit\maxdimen
    \m@th
    \ialign{##\crcr\hidewidth\smash{$-$}\hidewidth\crcr$\pitchfork$\crcr}
  }%
}
\makeatother

\newcommand{\TODO}[1]{\text{\textbf{TODO: }{#1}}}
\newcommand{\TODOD}[1]{\begin{center}\large{\TODO{#1}}\end{center}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator{\Id}{id}

\begin{document}

\maketitle

\begin{theorem}
Let \(S, Y \supseteq Z\) be manifolds and \(X\) be a compact manifold with boundary. Let \(F: X \times S \to Y\) be smooth, and define \(f_s(x) = F(x, s): X \to Y\). Then if \(F\) and \(\partial F\) are transversal to \(Z\), for almost all \(s in S\), \(f^s \transv Z\) and \(\partial f^s \transv Z\).
\end{theorem}
\begin{proof}
We have
\[F \transv Z \implies dF_{x, s}T_{x, s}X \times S + T_z(Z) = T_z(Y)\]
We need to show that ``only the \(X\) part matters", i.e. that for almost all \(s\),
\[df_x^sT_x(X) + T_z(Z) = T_z(Y)\]
So how do we know if \(s\) is a ``good \(s\)?" If we want \(s\) to be a regular value, what must it be a regular value of? Well, one candidate is the projection map \(\pi: W \to S\), where \(W = F^{-1}(Z)\). We know almost every \(s\) has this property by Sard's theorem, so all we need to do is show that this property implies transversality. So we want
\[\forall a \in T_z(Y), \exists b \in dF_{x, s}T_{x, s}(X \times S), a - b \in T_z(Z)\]
We can write, for \(u \in T_x(X), e \in T_s(S)\)
\[b = dF_{x, s}(u, e)\]
We want to get this \(e\) to zero, while maintaining that the difference goes to \(T_z(Z)\). Since \(s\) is a regular value for \(\pi\), we have that
\[\pi \implies \exists (w, e) \in T_{x, s}(W), d\pi(w, e) = e\]
So we pick \(v = u - w \in T_x(X)\). We claim that
\[df_x^s(v) - a \in T_z(Z)\]
We have that
\[df_x^s(v) - a = dF_{(x, s)}(v, 0) - a = dF_{(x, s)}((u, e) - (w, e)) - a\]
We have
\[dF_{(x, s)}(v, e) - a \in T_z(Z), \quad \land \quad dF_{(x, s)}(w, e) \in T_z(Z)\]
implying the difference is in \(T_z(Z)\), as desired. Now, we repeat with \(\partial f\) as \(f\), \(\partial F\) as \(F\) and \(\pi\) as \(\partial \pi\), and we get the desired result.
\end{proof}
Now, this result is a little bit technical, but quite powerful.

Let's say \(f: X \to Y\) is a smooth map, where \(Y \subseteq \reals^n\) has no boundary. Then there exists an open ball \(S\) in \(\reals^n\) such that we can define
\[F: X \times S \to Y\]
where \(F(x, 0) = f(x)\) such that \(s \mapsto F(x, s)\) is a submersion for every \(x \in X\), implying \(F, \partial F\) are transversal to \(Z\) for any \(Z \subseteq Y\).

If \(Y\) is compact, we can find some \(\epsilon\) such that a map \(c: U_{\epsilon}(Y) \to Y\) given by the closest point on the projection from \(U_{\epsilon} \to Y\), is defined. This is my way of wiggling things around, but any way works. We'll prove for the compact case, but it's not strictly necessary.

\begin{proof}
  Let \(S = B_1(0) \in \reals^n\). Define
  \[F(x, s) = \pi(f(x) + \epsilon s)\]
  Then
  \[G^x(s) = F(x, s)\]
  is a submersion for any \(v \in T_y(Y)\). Choose \(s = tv\). Then
  \[dG^x(s) = v\]
  \TODOD{fix}
\end{proof}

\begin{corollary}
  For every \(Z \subseteq Y\) and almost all \(s\), \(f_s\) is transversal to \(Z\)
\end{corollary}
\begin{corollary}
  If \(f: X \to Y\), \(Z \subseteq Y\), we can find \(g\) homotopic to \(f\) such that \(g \transv Z\), \(\partial g \transv Z\)
\end{corollary}
Oftentimes, our map is nice in one region, and we just want to ``fix" it everywhere else. For example, we may have \(C \subseteq X\) and \(f \transv Z\) on \(C\), i.e. for \(f(x) \in Z, x \in C\).
\begin{theorem}
  If \(f: X \to Y\) and \(f, \partial f \transv Z\) on \(C\), we can find some \(g: X \to Y\) such that \(g\) is homotopic to \(f\), \(g|_U = f|_U\) on some neighborhood \(U\) of \(C\), and \(g \transv Z\) and \(\partial g \transv Z\).
\end{theorem}
It's the same proof, just much longer. But we get a cool corollary!
\begin{corollary}
  If \(f: X \to Y\) and \(\partial f \transv Z\), then there exists a \(g \transv Z\) homotopic to \(f\) such that, where \(U\) is a neighborhood of \(\partial X\), \(g|_U = f|_U\).
\end{corollary}
The idea is basically, if a map is bad, we can usually fix it, and paying pretty much nothing. When you have all this, you can do topology.

\end{document}
